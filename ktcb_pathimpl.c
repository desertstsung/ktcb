#include "ktcb_pathimpl.h"

char *ktcb_adirname(char *filename)
{
	char *dst = NULL;
	
	asprintf(&dst, "%.*s", (int) (strlen(filename) - strlen(ktcb_basename(filename))), filename);
	
	return dst;
}

char *ktcb_apathcat(char *dir, char *name, char *suffix)
{
	char *dst = NULL;
	
	asprintf(&dst, "%s/%s%s", dir, name, suffix);
	
	return dst;
}

