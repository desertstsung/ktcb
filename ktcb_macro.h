#ifndef KTCB_MACRO_H
#define KTCB_MACRO_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>


/*
 * Safer free macro fn
 */
#define KTCB_FREE(ptr) \
	do { \
		if (ptr) { \
			free(ptr); \
			ptr = NULL; \
		} \
	} while (0)


/*
 * Print messages with leading current time and an ending '\n'
 */
#define KTCB___ECHOWITHTIME(stream, ...) \
	do { \
		time_t ktcb_global_time; \
		time(&ktcb_global_time); \
		fprintf(stream, "|%15.15s| ", ctime(&ktcb_global_time)+4); \
		fprintf(stream, __VA_ARGS__); \
		fprintf(stream, "\n"); \
	} while (0)
#define KTCB_ECHOWITHTIME(...) KTCB___ECHOWITHTIME(stdout, __VA_ARGS__)


/*
 * Error prompt
 */
#define KTCB_ERRECHOWITHTIME(...) KTCB___ECHOWITHTIME(stderr, __VA_ARGS__)
#define KTCB_ERRLOC               KTCB_ERRECHOWITHTIME("File: %s, Fn: %s, Ln: %d", __FILE__, __FUNCTION__, __LINE__)

#define KTCB_ERROPEN(fname) \
	do { \
		KTCB_ERRECHOWITHTIME("ERROR %d %s: %s", errno, strerror(errno), fname); \
		KTCB_ERRLOC; \
	} while (0)

#define KTCB_ERRFIO(fd) \
	do { \
		close(fd); \
		KTCB_ERRECHOWITHTIME("ERROR %d %s", errno, strerror(errno)); \
		KTCB_ERRLOC; \
	} while (0)

#define KTCB_ERRMEM(ptr) \
	do { \
		KTCB_FREE(ptr); \
		KTCB_ERRECHOWITHTIME("MEM PANIC %d %s", errno, strerror(errno)); \
		KTCB_ERRLOC; \
	} while (0)

#define KTCB_EXITWITHMSG(...) \
	do { \
		KTCB_ERRLOC; \
		KTCB_ERRECHOWITHTIME(__VA_ARGS__); \
		exit(1); \
	} while (0)


#endif

