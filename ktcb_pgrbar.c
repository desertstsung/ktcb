#include "ktcb_pgrbar.h"

struct ktcb_pgrbar *ktcb_barinit(void)
{
	struct ktcb_pgrbar *pbar = ktcb_malloc(sizeof(struct ktcb_pgrbar));
	
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	
	pbar->ncol = w.ws_col;
	pbar->bar = ktcb_malloc(pbar->ncol+1);
	pbar->nbrc = 2;
	pbar->npercent = 7;
	pbar->npercenta = pbar->npercent+1;
	pbar->nsymbol = 0.9*(pbar->ncol - pbar->nbrc - pbar->npercent);
	
	pbar->prevspercent = ktcb_malloc(pbar->npercenta);
	pbar->nextspercent = ktcb_malloc(pbar->npercenta);
	snprintf(pbar->prevspercent, pbar->npercenta, KTCB_PERCENTAGEFMT, 0.);
	
	uint16_t ipos = 0;
	ipos[pbar->bar] = KTCB_BARLEFT;
	for (++ipos; ipos < pbar->nsymbol+1; ++ipos)
		ipos[pbar->bar] = KTCB_BARSYM2;
	ipos[pbar->bar] = KTCB_BARRIGHT;
	for (++ipos; ipos < w.ws_col-pbar->npercent; ++ipos)
		ipos[pbar->bar] = KTCB_BARSPC;
	snprintf(pbar->bar+ipos, pbar->npercenta, "%s", pbar->prevspercent);
	
	pbar->spercentpos = pbar->bar+ipos;
	printf("%s", pbar->bar);
	
	return pbar;
}

int ktcb_barupdate(struct ktcb_pgrbar *pbar, float curper)
{
	snprintf(pbar->nextspercent, pbar->npercenta, KTCB_PERCENTAGEFMT, 100*curper);
	if (strcmp(pbar->nextspercent, pbar->prevspercent)) {
		pbar->ndone = pbar->nsymbol*curper;
		
		for (uint16_t ipos = 1; ipos < pbar->ndone+1; ++ipos)
			pbar->bar[ipos] = KTCB_BARSYM1;
		snprintf(pbar->spercentpos, pbar->npercenta, "%s", pbar->nextspercent);
		
		fflush(stdout);
		printf("\r%s", pbar->bar);
		
		memcpy(pbar->prevspercent, pbar->nextspercent, 8);
	}
	
	return 0;
}

int ktcb_barfinish(struct ktcb_pgrbar **ppbar)
{
	(*ppbar)->prevspercent[0] = '0';
	ktcb_barupdate(*ppbar, 1);
	printf("\n");
	(*ppbar)->spercentpos = NULL;
	ktcb_freethemall(4, &(*ppbar)->bar, &(*ppbar)->prevspercent, &(*ppbar)->nextspercent, ppbar);
	
	return 0;
}

