#include "ktcb_array.h"

static uint64_t dim2index(struct ktcb_array *array, uint64_t dim[])
{
	uint64_t index = 0;
	
	for (uint8_t idim = 0; idim < array->ndim; ++idim)
		index += array->dimscoef[idim] * dim[idim];
	
	return index;
}

static inline uint8_t isempty(struct ktcb_array *array)
{
	return !(array->data || array->dims || array->dimscoef);
}

static inline double getindex_u8(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U8)) ? NAN : (array->coef * *(((uint8_t *) array->data) + index));
}

static inline double getdata_u8(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_u8(array, dim2index(array, dim));
}

static inline double getindex_i8(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I8)) ? NAN : (array->coef * *(((int8_t *) array->data) + index));
}

static inline double getdata_i8(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_i8(array, dim2index(array, dim));
}

static inline double getindex_u16(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U16)) ? NAN : (array->coef * *(((uint16_t *) array->data) + index));
}

static inline double getdata_u16(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_u16(array, dim2index(array, dim));
}

static inline double getindex_i16(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I16)) ? NAN : (array->coef * *(((int16_t *) array->data) + index));
}

static inline double getdata_i16(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_i16(array, dim2index(array, dim));
}

static inline double getindex_u32(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U32)) ? NAN : (array->coef * *(((uint32_t *) array->data) + index));
}

static inline double getdata_u32(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_u32(array, dim2index(array, dim));
}

static inline double getindex_i32(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I32)) ? NAN : (array->coef * *(((int32_t *) array->data) + index));
}

static inline double getdata_i32(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_i32(array, dim2index(array, dim));
}

static inline double getindex_u64(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U64)) ? NAN : (array->coef * *(((uint64_t *) array->data) + index));
}

static inline double getdata_u64(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_u64(array, dim2index(array, dim));
}

static inline double getindex_i64(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I64)) ? NAN : (array->coef * *(((int64_t *) array->data) + index));
}

static inline double getdata_i64(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_i64(array, dim2index(array, dim));
}

static inline double getindex_f32(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_F32)) ? NAN : (array->coef * *(((float *) array->data) + index));
}

static inline double getdata_f32(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_f32(array, dim2index(array, dim));
}

static inline double getindex_f64(struct ktcb_array *array, uint64_t index)
{
	return ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_F64)) ? NAN : (array->coef * *(((double *) array->data) + index));
}

static inline double getdata_f64(struct ktcb_array *array, uint64_t dim[])
{
	return getindex_f64(array, dim2index(array, dim));
}

static void setindex_u8(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U8))
		return;
	
	*(((uint8_t *) array->data) + index) = value;
}

static void setdata_u8(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_u8(array, dim2index(array, dim), value);
}

static void setindex_i8(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I8))
		return;
	
	*(((int8_t *) array->data) + index) = value;
}

static void setdata_i8(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_i8(array, dim2index(array, dim), value);
}

static void setindex_u16(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U16))
		return;
	
	*(((uint16_t *) array->data) + index) = value;
}

static void setdata_u16(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_u16(array, dim2index(array, dim), value);
}

static void setindex_i16(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I16))
		return;
	
	*(((int16_t *) array->data) + index) = value;
}

static void setdata_i16(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_i16(array, dim2index(array, dim), value);
}

static void setindex_u32(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U32))
		return;
	
	*(((uint32_t *) array->data) + index) = value;
}

static void setdata_u32(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_u32(array, dim2index(array, dim), value);
}

static void setindex_i32(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I32))
		return;
	
	*(((int32_t *) array->data) + index) = value;
}

static void setdata_i32(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_i32(array, dim2index(array, dim), value);
}

static void setindex_u64(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_U64))
		return;
	
	*(((uint64_t *) array->data) + index) = value;
}

static void setdata_u64(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_u64(array, dim2index(array, dim), value);
}

static void setindex_i64(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_I64))
		return;
	
	*(((int64_t *) array->data) + index) = value;
}

static void setdata_i64(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_i64(array, dim2index(array, dim), value);
}

static void setindex_f32(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_F32))
		return;
	
	*(((float *) array->data) + index) = value;
}

static void setdata_f32(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_f32(array, dim2index(array, dim), value);
}

static void setindex_f64(struct ktcb_array *array, uint64_t index, double value)
{
	if ((isempty(array)) || (array->type != KTCB_ARRAY_TYPE_F64))
		return;
	
	*(((double *) array->data) + index) = value;
}

static void setdata_f64(struct ktcb_array *array, uint64_t dim[], double value)
{
	setindex_f64(array, dim2index(array, dim), value);
}

static void destroyarray(struct ktcb_array **array)
{
	ktcb_freethemall(4, &(*array)->data, &(*array)->dims, &(*array)->dimscoef, array);
}

static void *realloc_array(void *ptr, size_t nmemb, size_t size)
{
	if ((0 == size) || (0 == nmemb) || (nmemb > SIZE_MAX / size))
		return NULL;
	return realloc(ptr, nmemb*size);
}

static void settype(struct ktcb_array *array, enum ktcb_array_type typecode)
{
	if (array->type != typecode) {
		array->type = typecode;
		
		switch (typecode) {
		case KTCB_ARRAY_TYPE_U8:  array->getdata  = getdata_u8 ;
		                          array->setdata  = setdata_u8 ;
		                          array->getindex = getindex_u8;
		                          array->setindex = setindex_u8;
		                          break;
		case KTCB_ARRAY_TYPE_I8:  array->getdata  = getdata_i8 ;
		                          array->setdata  = setdata_i8 ;
		                          array->getindex = getindex_i8;
		                          array->setindex = setindex_i8;
		                          break;
		case KTCB_ARRAY_TYPE_U16: array->getdata  = getdata_u16 ;
		                          array->setdata  = setdata_u16 ;
		                          array->getindex = getindex_u16;
		                          array->setindex = setindex_u16;
		                          break;
		case KTCB_ARRAY_TYPE_I16: array->getdata  = getdata_i16 ;
		                          array->setdata  = setdata_i16 ;
		                          array->getindex = getindex_i16;
		                          array->setindex = setindex_i16;
		                          break;
		case KTCB_ARRAY_TYPE_U32: array->getdata  = getdata_u32 ;
		                          array->setdata  = setdata_u32 ;
		                          array->getindex = getindex_u32;
		                          array->setindex = setindex_u32;
		                          break;
		case KTCB_ARRAY_TYPE_I32: array->getdata  = getdata_i32 ;
		                          array->setdata  = setdata_i32 ;
		                          array->getindex = getindex_i32;
		                          array->setindex = setindex_i32;
		                          break;
		case KTCB_ARRAY_TYPE_U64: array->getdata  = getdata_u64 ;
		                          array->setdata  = setdata_u64 ;
		                          array->getindex = getindex_u64;
		                          array->setindex = setindex_u64;
		                          break;
		case KTCB_ARRAY_TYPE_I64: array->getdata  = getdata_i64 ;
		                          array->setdata  = setdata_i64 ;
		                          array->getindex = getindex_i64;
		                          array->setindex = setindex_i64;
		                          break;
		case KTCB_ARRAY_TYPE_F32: array->getdata  = getdata_f32 ;
		                          array->setdata  = setdata_f32 ;
		                          array->getindex = getindex_f32;
		                          array->setindex = setindex_f32;
		                          break;
		case KTCB_ARRAY_TYPE_F64: array->getdata  = getdata_f64 ;
		                          array->setdata  = setdata_f64 ;
		                          array->getindex = getindex_f64;
		                          array->setindex = setindex_f64;
		                          break;
		case KTCB_ARRAY_TYPE_UNINIT:
		default: break;
		} 
		
		if (typecode)
			array->data = realloc_array(array->data, array->nelements, (array->type%100)/2);
		else
			KTCB_FREE(array->data);
	}
}

static void setdim(struct ktcb_array *array, uint8_t ndim, uint64_t dims[])
{
	if (!ndim)
		return;
	
	array->ndim = ndim;
	array->dimscoef = realloc_array(array->dimscoef, ndim, sizeof(uint64_t));
	array->dimscoef[--ndim] = 1;
	while (ndim-->0) {
		array->dimscoef[ndim] = array->dimscoef[ndim+1] * dims[ndim+1];
	}
	array->nelements = array->dimscoef[0] * dims[0];
	array->dims = realloc_array(array->dims, ndim, sizeof(uint64_t));
	memcpy(array->dims, dims, sizeof(uint64_t[ndim]));
	
	if (array->type)
		array->data = realloc_array(array->data, array->nelements, (array->type%100)/2);
}

static struct ktcb_array *init_nullarray()
{
	struct ktcb_array *ret = ktcb_malloc(sizeof(struct ktcb_array));
	
	ret->coef = 1;
	ret->ndim = 0;
	ret->nelements = 0;
	
	ret->data = NULL;
	ret->dims = NULL;
	ret->dimscoef = NULL;
	
	ret->type = KTCB_ARRAY_TYPE_UNINIT;
	
	ret->getdata  = NULL;
	ret->setdata  = NULL;
	ret->getindex = NULL;
	ret->setindex = NULL;
	
	ret->setdim  = setdim;
	ret->settype = settype;
	ret->destroy = destroyarray;
	
	return ret;
}

struct ktcb_array *ktcb_array_init(uint8_t ndim, uint64_t dims[], enum ktcb_array_type typecode)
{
	struct ktcb_array *ret = init_nullarray();
	
	settype(ret, typecode);
	setdim(ret, ndim, dims);
	
	return ret;
}

int ktcb_array_destroyall(uint8_t n, ...)
{
	va_list ap;
	struct ktcb_array **p;
	
	va_start(ap, n);
	while (n-- > 0) {
		p = va_arg(ap, struct ktcb_array **);
		(*p)->destroy(p);
	}
	va_end(ap);
	
	return 0;
}

