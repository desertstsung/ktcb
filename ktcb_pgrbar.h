#ifndef KTCB_PGRBAR_H
#define KTCB_PGRBAR_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "ktcb_memman.h"


#define KTCB_PERCENTAGEFMT "%6.2f%%"
#define KTCB_BARLEFT  '['
#define KTCB_BARSYM1  '#'
#define KTCB_BARSYM2  '-'
#define KTCB_BARRIGHT ']'
#define KTCB_BARSPC   ' '

struct ktcb_pgrbar {
	char *bar;
	
	uint16_t ncol;
	uint16_t nsymbol;
	uint16_t nbrc;
	uint16_t ndone;
	uint16_t npercent;
	uint16_t npercenta;
	
	char *prevspercent;
	char *nextspercent;
	
	char *spercentpos;
};

extern struct ktcb_pgrbar *ktcb_barinit(void);
extern int ktcb_barupdate(struct ktcb_pgrbar *, float);
extern int ktcb_barfinish(struct ktcb_pgrbar **);


#endif

