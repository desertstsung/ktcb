#ifndef KTCB_PATHIMPL_H
#define KTCB_PATHIMPL_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <libgen.h>

#include "ktcb_memman.h"


extern char *ktcb_adirname(char *);
extern char *ktcb_apathcat(char *, char *, char *);
#define ktcb_basename(...) basename(__VA_ARGS__)


#endif

