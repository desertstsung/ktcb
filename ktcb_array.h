#ifndef KTCB_ARRAY_H
#define KTCB_ARRAY_H

#include "ktcb_macro.h"
#include "ktcb_memman.h"


enum ktcb_array_type {
KTCB_ARRAY_TYPE_UNINIT,
KTCB_ARRAY_TYPE_U8 = 2,
KTCB_ARRAY_TYPE_I8,
KTCB_ARRAY_TYPE_U16 = 4,
KTCB_ARRAY_TYPE_I16,
KTCB_ARRAY_TYPE_U32 = 8,
KTCB_ARRAY_TYPE_I32,
KTCB_ARRAY_TYPE_U64 = 16,
KTCB_ARRAY_TYPE_I64,
KTCB_ARRAY_TYPE_F32 = 108,
KTCB_ARRAY_TYPE_F64 = 116,
};

struct ktcb_array {
	void *data;
	double coef;
	uint64_t nelements;
	uint8_t ndim;
	uint64_t *dims;
	uint64_t *dimscoef;
	enum ktcb_array_type type;
	
	double (*getdata) (struct ktcb_array *, uint64_t[]);
	void (*setdata) (struct ktcb_array *, uint64_t[], double);
	double (*getindex) (struct ktcb_array *, uint64_t);
	void (*setindex) (struct ktcb_array *, uint64_t, double);
	void (*setdim) (struct ktcb_array *, uint8_t, uint64_t[]);
	void (*settype) (struct ktcb_array *, enum ktcb_array_type);
	void (*destroy) (struct ktcb_array **);
};

#ifndef NAN
#define NAN 0./0.
#endif

extern struct ktcb_array *ktcb_array_init(uint8_t, uint64_t[], enum ktcb_array_type);
extern int ktcb_array_destroyall(uint8_t, ...);


#endif

