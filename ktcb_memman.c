#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "ktcb_memman.h"


void *ktcb_malloc(size_t size)
{
	void *ptr = malloc(size);
	
	if (ptr) {
		return ptr;
	} else {
		KTCB_ERRMEM(ptr);
		exit(1);//TODO exit code macro
	}
}

int ktcb_freethemall(uint8_t n, ...)
{
	va_list ap;
	void **p;
	
	va_start(ap, n);
	while (n-- > 0) {
		p = va_arg(ap, void **);
		KTCB_FREE(*p);
	}
	va_end(ap);
	
	return 0;
}

