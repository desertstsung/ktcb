#ifndef KTCB_MEMMAN_H
#define KTCB_MEMMAN_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdint.h>
#include <stdarg.h>

#include "ktcb_macro.h"


extern void *ktcb_malloc(size_t);
extern int ktcb_freethemall(uint8_t, ...);
#define ktcb_allocstack(...) alloca(__VA_ARGS__)


#endif

